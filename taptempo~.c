// ================================================================================
// name: taptempo~.c
// desc: extern for pd
//          tries to estimate some tempo based on bangs in the input
//
//
//
// author: jennifer hsu
// date: fall 2013
// =================================================================================

#include "m_pd.h"
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define TAPS_TO_AVERAGE 5


// struct type to hold all variables that have to do with the delayline~ object
typedef struct _taptempo
{
    t_object x_obj;
    t_float sr;
    t_int inBang;
    t_int countBang;
    t_float prevTime;
    t_float * timeDiffs;
    t_float bpmEstimate;
} t_taptempo;

// taptempo~ class that will be created in 'setup'
// and used in 'new' to create new instances
t_class *taptempo_class;

/* function prototypes */
static t_int *taptempo_perform(t_int *w);
static void taptempo_dsp(t_taptempo *x_obj, t_signal **sp);
static void *taptempo_new(void);
void taptempo_tilde_setup(void);


// perform function (audio callback)
static t_int *taptempo_perform(t_int *w)
{
    
    // extract data
    t_taptempo *x_obj = (t_taptempo *)(w[1]);
    t_float *in = (t_float *)(w[2]);
    t_float *out = (t_float *)(w[3]);
    int bufferSize = (int)(w[4]);
    
    
    
    // output
    int samp;
    for(samp = 0; samp < bufferSize; samp++)
    {
        *(out+samp) = *(in+samp);
    }
    
    
    return (w+5);
}

// called to start dsp, register perform function
static void taptempo_dsp(t_taptempo *x_obj, t_signal **sp)
{
    // save this here because I can't get it to work in the add function...
    x_obj->sr = sp[0]->s_sr;
    // add the perform function
    dsp_add(taptempo_perform, 4, x_obj, sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
    
}

void receivedBang(t_taptempo *x_obj)
{
    clock_t t;
    t = clock();
    double timeNow = clock_getlogicaltime(); // get time now in ms
    double timePassed;
    
    
    if(x_obj->countBang == 0)
    {
        timePassed = 0;
    } else
    {
        timePassed = clock_gettimesince(x_obj->prevTime); // get time since last bang in ms
    }
    
    *(x_obj->timeDiffs + x_obj->countBang) = timePassed;
    
    x_obj->countBang++;
    x_obj->prevTime = timeNow;
    
    
    if(x_obj->countBang >= TAPS_TO_AVERAGE)
    {
        int i;
        for(i = 0; i < TAPS_TO_AVERAGE; i++)
        {
            x_obj->bpmEstimate += *(x_obj->timeDiffs+i);
        }
        x_obj->bpmEstimate /= (TAPS_TO_AVERAGE-1);
        x_obj->bpmEstimate = 1.0f/x_obj->bpmEstimate * 60.0f * 1000.0f;
        
        x_obj->countBang = 0;
        post("estimated bpm: %f", x_obj->bpmEstimate);
    }
    
}


// new function that is called everytime we create a new taptempo object in pd
static void *taptempo_new(void)
{
    // create object from class
    t_taptempo *x_obj = (t_taptempo *)pd_new(taptempo_class);
    
    // create a signal outlet for the delayed signal
    outlet_new(&x_obj->x_obj, gensym("signal"));
    
    x_obj->sr = 0.0f;
    x_obj->countBang = 0;
    
    x_obj->timeDiffs = malloc(TAPS_TO_AVERAGE * sizeof(t_float));
    
    
    return (x_obj);
}

// called when Pd is first loaded and tells Pd how to load the class
void taptempo_tilde_setup(void)
{
    taptempo_class = class_new(gensym("taptempo~"), (t_newmethod)taptempo_new, 0,
                                sizeof(t_taptempo), 0, 0);
    
    // magic to declare the leftmost inlet the main inlet that will take a signal
    // installs delayTime as the leftmost inlet float
    CLASS_MAINSIGNALIN(taptempo_class, t_taptempo, sr);
    
    // register method that will be called when dsp is turned on
    class_addmethod(taptempo_class, (t_method)taptempo_dsp, gensym("dsp"), (t_atomtype)0);
    
    class_addbang(taptempo_class, (t_method)receivedBang);
    
}







